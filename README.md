# Subopt

## What it is

Here's a variation on getsubopt.

Say you have a program, and it takes an option --formatting-options,
which itself takes a list of arguments. The user might write:
"--fmt=lines=4,colour". That format, keywords followed by an optional
equals and a val delimited by commas is the standard way to do
suboptions.

getsuboption only lets you do what I'll call "long" options: "lines",
"colour". This lets you do those and also "short" options.

This means a user of your program can write instead of the above
example, "--fmt=l=4,c". Unlike regular options, the "=" after the l is
necessary, because there's nothing to signify the start of options. I
suppose I could do some trickery to make unnecessary but I won't rush
to.

## How it works

There's an example in ./example/example.c, and a big comment above
so\_get\_subopt.

## Comparison with getsubopt

getsubopt is destructive, ie it writes zeros into your string. This is
fine most of the time, but at other times it means you have to
allocate a new string, which just clutters things up.

The downside of not marking the end of values with zeroes is this
function returns the value in two variables, value\_s and value\_e.

getopt updates its pointer into the string. I mean it takes a
char \*\* and increments it, meaning you have to declare a second char
if you don't want to lose the start of your string.

The downside of my not updating that pointer is I have to return the
position through value\_e, which is a pretty weird thing to do. It
means you need to set value\_e to your string before you call the
function.

getsubopt indicates errors by returning -1 and setting the value
string to NULL. This uses return values below -1 to indicate the kind
of error. -1 (SO\_SUCCESS) means it's reached the end of the string.
There's a so\_strerror function you can use to get a string describing
the error.

Getopt doesn't do optional arguments. Well, it does. I just doesn't do
them officially. Here, you can say SO\_REQUIRED\_ARGUMENT and
SO\_OPTIONAL\_ARGUMENT.

## Value of value\_s and value\_e

If SO\_REQUIRED\_ARGUMENT or SO\_OPTIONAL\_ARGUMENT and one was found
they are the start and end of the value.

If SO\_OPTIONAL\_ARGUMENT and one was not found, both are set to the
end of the keyword. So if your string was "optional-arg,another-arg",
both would be on the comma.

If no argument required or an error has occured, they point to the
offending token so you can print them out.

## Detecting errors

The return value will tell you. If you've encountered a non-existant
keyword, you can get it through value\_s and value\_e.

```
if (index < -1)
	fprintf (stderr, "%.*s not recognised: %s\n", (int)
		(value_e - value_s), value_s, so_strerror (index));
```

## More

If you pass 0 as a short option or NULL as a long option, that option
will be skipped.

## Install, etc

```
git clone 'https://gitlab.com/objection/subopt.git
cd subopt
meson build
ninja -Cbuild install
```

## However

You don't need to install it. Just include it as a submodule if you
want (`git submodule add 'https://gitlab.com/objection/subopt.git
subopt`). Or you can just include and compile the files.

