#include "subopt.h"
#include <ctype.h>
#include <string.h>
#include <threads.h>
#include <stdio.h>

#define $each(_item, _arr, _n) \
	for (__typeof__ (_arr[0]) *_item = _arr; _item < _arr + _n; _item++)
#define $fori(_idx, _max) 																\
	for (typeof (_max + 0) _idx = 0; _idx < _max; _idx++)

// I'll just make this global. I could make a strerror function to
// hide them. I won't.
#define x(a, b) b,
static char *so_exit_status_strs[N_SO_EXIT_STATUSES + 1] = {SO_EXIT_STATUS};
#undef x


enum so_kind {
	SO_KIND_NOTHING,
	SO_KIND_COMMA = ',',
	SO_KIND_EQUALS = '=',
	SO_KIND_VALUE,
	SO_KIND_KEYWORD,
	SO_KIND_MORE_THAN_ONE_MATCH,
	SO_KIND_TOO_LONG_TOKEN,
	SO_KIND_END,
};

struct so_token {
	char *s, *e;
	enum so_kind kind;
	int keyword_id;
};

thread_local void *user;
thread_local int (*match_func) (const char *, const char *, void *);

static int match_str (const char *a, const char *b, void *) {
	return strcmp (a, b);
}

static int so_match_long_keyword (struct so_token *t, struct subopt *subopts,
		size_t n_subopts) {
	int longest_len = 0;
	int longest = -1;
	int r = -1;
	int n_tops = 0;
	int perfect_match = -1;
	char token_buf[BUFSIZ] = {};
	int len = t->e - t->s;
	if (len >= BUFSIZ)
		return -3;
	strncpy (token_buf, t->s, t->e - t->s);
	$fori (i, n_subopts) {
		if (subopts[i].lng && !match_func (token_buf, subopts[i].lng, user)) {
			if (len >= longest_len) {
				if (len > longest_len) {
					if (*t->e == 0 || *t->e == ',')
						perfect_match = i;
					longest_len = len;
					longest = i;
					n_tops = 0;
				}
				n_tops++;
			}
		}
	}
	if (n_tops > 1) {
		if (perfect_match != -1) {
			t->e = t->s + longest_len;
			r = longest;
		} else

			// This is naff.
			return -2;
	}

	if (longest != -1) {
		t->e = t->s + longest_len;
		r = longest;
	}
	return r;
}

static int so_match_keyword (struct so_token *t, struct subopt *subopts,
		size_t n_subopts) {

	if (t->e - t->s == 1) {
		$each (subopt, subopts, n_subopts) {

			if (!*t->s)
				continue;
			if (*t->s == subopt->shrt)
				return subopt - subopts;
		}
	}
	return so_match_long_keyword (t, subopts, n_subopts);
}

static struct so_token so_get_token (char *p, char *e, struct subopt *subopts,
		size_t n_subopts) {
	struct so_token r = {.s = p};
	while (r.s <= e && isspace (*r.s)) r.s++;
	if (r.s == e) {
		r.e = r.s;
		r.kind = SO_KIND_END;
	}
	else {
		r.e = r.s + 1;
		switch (*r.s) {
			case '=':
				// fallthrough
			case ',':
				r.kind = *r.s;
				break;
			default:
				while (r.e < e && *r.e != '=' && *r.e != ',') r.e++;
				r.keyword_id = so_match_keyword (&r, subopts, n_subopts);

				// This is pretty naff, say I.
				if (r.keyword_id == -3)      r.kind = SO_KIND_TOO_LONG_TOKEN;
				if (r.keyword_id == -2)      r.kind = SO_KIND_MORE_THAN_ONE_MATCH;
				else if (r.keyword_id != -1) r.kind = SO_KIND_KEYWORD;
				else                         r.kind = SO_KIND_VALUE;
				break;
		}
	}
	return r;
}

#define GET_TOKEN() \
	*t = so_get_token (t->e, e, subopts, n_subopts);

static int so_parse_keyword (struct so_token *t, char *e, struct subopt *subopts,
		size_t n_subopts, bool *value_found) {
	int r = t->keyword_id;
	if (subopts[t->keyword_id].argument) {
		enum so_argument argument = subopts[t->keyword_id].argument;
		GET_TOKEN ();
		if (t->kind == SO_KIND_EQUALS) {
			GET_TOKEN ();
			if (t->kind != SO_KIND_VALUE) {
				if (argument == SO_OPTIONAL_ARGUMENT) {

					// Make them both the same, so you get a
					// zero-length string and not ','.
					r = t->keyword_id;
				}
			} else
				*value_found = 1;
		} else if (argument == SO_REQUIRED_ARGUMENT) {
			return SO_MISSING_VALUE;
		}
	}
	return r;
}

// See so_get for most of this. The last of it is:
//
// match_func: a function that returns 0 for a correct match
// on a token. That function takes two const chars, like strcmp, and
// also a "user", which you might want to use to indicate errors in
// your match func.
//
// user: The user void *, passed to match_func.
//
// match_func could take a len, but doesn't because I just want the
// library to not be a pain to use. If it took a len, you'd have to
// remember that when doing tour parsing. As it is, this library
// copies the token to a buffer of BUFSIZ. A token of >= BUFSIZ is an
// error.
int so_get_with_match_func (char **value_s, char **value_e, char *str_end,
		struct subopt *subopts, size_t n_subopts, int (*_match_func) (const char *,
			const char *, void *), void *_user) {
	user = _user;
	match_func = _match_func;
	if (!str_end) str_end = strchr (*value_e, '\0');
	if (*value_e == str_end)
		return SO_SUCCESS;

	struct so_token t = so_get_token (*value_e, str_end, subopts, n_subopts);
	bool value_found = 0;
	int r = -1;
	switch (t.kind) {
		case SO_KIND_COMMA:
			*value_e += 1;
			r = so_get_with_match_func (value_s, value_e, str_end, subopts, n_subopts,
					match_func, 0);
			t.s = *value_s, t.e = *value_e;
			break;
		case SO_KIND_KEYWORD:
			r = so_parse_keyword (&t, str_end, subopts, n_subopts, &value_found);
			break;
		case SO_KIND_VALUE:               r = SO_UNRECOGNISED_OPTION; break;
		case SO_KIND_MORE_THAN_ONE_MATCH: r = SO_MORE_THAN_ONE_MATCH; break;
		case SO_KIND_TOO_LONG_TOKEN:      r = SO_TOO_LONG_TOKEN; break;
		case SO_KIND_END:                 r = SO_SUCCESS; break;
		default:
			break;
	}
	if (r >= 0 && subopts[r].argument == SO_OPTIONAL_ARGUMENT
			&& !value_found) {

		// An optional argument is there if value_s isn't 0. This is
		// obviously a bit shit. Or is it?
		*value_s = 0;
		*value_e = t.e;
	} else
		*value_s = t.s, *value_e = t.e;
	return r;
}

// You call this function in a loop. If it finds a match, it returns
// the index into the subopts array of the match. If not, it returns a
// negative number, an error code. -1, SO_SUCCESS means it's
// encountered the end of the string. To get a string for the error,
// use the function so_strerror.
//
// If the function finds a value, the start of the value is put into
// *value_s and the end is put into *value_e. If it doesn't find a
// value, both value_e and value_s are set to the last thing parsed.
// This means in your while loop, s should be value_e and value_e
// should be set beforehand to the start of the string. You might
// think I'd use s for this job, but I'd rather you didn't lose your
// pointer to that start of your string.
//
// Take a look at ./example/example.c. chmod u+x the file and run it
// to compile.
//
// value_s: This is where the value gets put, if the is one.
// value_e: This is the end of the value, but also
// e: end of parsing or NULL if you know the string is
// 		null-terminated.
// subopts: an array of suboptions. See subopt.h.
// n_subopts: the number of suboptions.
// value_s: the start of any found value is put in here. If it's
// 		not found it's 0.
// value_e: the end of any found value is put in here. The end of the
// 		keyword is put here so the string can continue to be parsed.
int so_get (char **value_s, char **value_e, char *str_end, struct subopt *subopts,
		size_t n_subopts) {
	return so_get_with_match_func (value_s, value_e, str_end, subopts, n_subopts,
			match_str, 0);
}

char *so_strerror (int so_errno) {
	return so_exit_status_strs[-so_errno - 1];
}
