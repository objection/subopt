#pragma once
#include <stdbool.h>
#include <stddef.h>

enum so_argument {
	SO_REQUIRED_ARGUMENT = 1,
	SO_OPTIONAL_ARGUMENT,
};

struct subopt {
	char *lng;
	char shrt;
	enum so_argument argument;
};

#define SO_EXIT_STATUS \
	x(SO_SUCCESS = -1, "Parse_successful") \
	x(SO_UNRECOGNISED_OPTION = -2, "Unrecognised option") \
	x(SO_MISSING_VALUE = -3, "Missing value") \
	x(SO_MISPLACED_EQUALS = -4, "Misplaced equals") \
	x(SO_MORE_THAN_ONE_MATCH = -5, "More than one match") \
	x(SO_TOO_LONG_TOKEN = -6, "Token too long") \
	x(N_SO_EXIT_STATUSES = 6, 0)
#define x(a, b) a,
enum so_exit_statuses {SO_EXIT_STATUS};
#undef x

int so_get (char **value_s, char **value_e, char *str_end,
		struct subopt *subopts, size_t n_subopts);
int so_get_with_match_func (char **value_s, char **value_e, char *str_end,
		struct subopt *subopts, size_t n_subopts, int (*_match_func) (const char *,
			const char *, void *), void *_user);
char *so_strerror (int so_errno);
